Propositions d’amendements à apporter au R.E.
Réglement des études, anciennement R.O.I. (Règlement d’Ordre Intérieur), aujourd’hui R.E.I.N.E.

Remarques prélabiales :
Conformément à l’Article 1 de ce présent règlement, ce texte est écrit en écriture inclu-sive : écriture dont «la forme» (grammaire, syntaxe, choix de termes) ne discrimine ou n’invisibilise aucune identité de genre.Les différentes formes typographiques d’écritures inclusives seront toutes repré-sentées afin de n’en discriminer aucune :• utilisation du tiret : tou-te-es• utilisation du point : tou.te.es • utilisation du point médian : tou•te•s• utilisation de la majuscule : touTEs• utilisation des parenthèses : tou(te) s (Nota Bene : celles-ci ne sont recomman-dées qu’à la condition où la personne présuppose une définition ouverte, incluante de ses individuations)• utilisation du slash : tou/te/s• utilisation des pronoms neutres : celleux, iel, iels• utilisation du féminin neutre : utilisation de la forme féminine systématiquement• utilisation de nouveaux termes inventés dans le but de suivre les prescriptions de l’Article 1.

Article 1 : Écriture inclusive
L’usage de l’écriture inclusive sera désor-mais obligatoire pour tout rendu écrit : rapport, mémoire, examen, procès verbal d’assemblée générale, procès verbal du CE, du CGP, communications de Sammy.L’usage de l’écriture inclusive sera également obligatoire pour tout post publié sur les réseaux sociaux de l’erg.Un cours sera dispensé à touTEs les étu-diantEs et enseignantEs, afin de permettre une meilleure pratique de cette écriture.Un autre cours verra le jour, celui de l’adaptation à l’oral de l’écriture inclusive, présentant pour les masters un module avancé d’invention de la langue incluante ou inclusive, pour lui permettre de se transformer en continu et ainsi s’ouvrir aux nouveautés qu’elle devra inclure.L’hégémonie du point du vue “objec-tif”, “impartial” et “neutre” sera punie (voir Article 19 : BDSM/fouet).

Article 1 bi : Féminin neutreL’usage du féminin neutre sera désormais obligatoire pour tous les cours oraux et communications orales.

Article 1 bis : Sur la question du langage en généralLecture obligatoire de Non, le masculin ne l’emporte pas sur le féminin ! d’Éliane ViennotApprendre à cœur le vocabulaire suivant de Reclaim The Nighthttps://reclaimthenightbruxelles.wordpress.com

Article 2 : Concernant la bibliothèque et ses ouvragesLorsque l’auteur s’identifie comme homme, cisgenre, hétérosexuel et blanc, les livres seront déplacés dans les archives, pour rappeler, d’une part, qu’il s’agit d’un point de vue parmi d’autres, d’autre part, que ce dernier est hégémonique. Une page d’avertissement devra être inclue dans chaque ouvrage quand les lecteur.ice.s souhaiteront consulter les dites œuvres.Des quotas strictes seront mis en place concernant la sélection des livres représentés. L’attention sera portée à la fois sur les sujets, le contexte d’écriture et les aut.eur.rice.s.La bibliothèque devra proposer dans ses rayons des ouvrages dont les sujets seront présents dans les proportions suivantes : • 20% autour des questions de genre, • 20% autour des questions Queer, • 20% sur les questions de féminisme, • 20% sur les questions d’afroféminisme,• 20% sur les questions de transféminisme,• 20% sur les questions de xenoféminisme,• 20% autour des questions de féminisme intersectionnel, • 20% autour des questions d’écoféminisme, • 20% autour des questions d’écosexualité, • 20%, autour des questions LGBT, • 20% autour des questions LGBTQQI2SPAA+ (à mettre à jour régulièrement). La liste n’est pas exhaustive.

Article 3 : MinervalUn minerval est imposé par la Fédération Wallonie-Bruxelles. Son montant est indexé et varie selon les années et le type d’études. Le prix du minerval sera ajusté pour chacun.e en fonction d’un coefficient qui sera attribué selon les critères suivants qui détermineront le taux de privilège :
◊ homme
◊ hétérosexuel.le
◊ cisgenre
◊ blanc.he
◊ corps normé.e
◊ valide◊ lettré.e
◊ classe moyenne et bourgeois.e
◊ carnivore
◊ humain.eTotal = .../ 10Ce coefficient est ensuite traduit en pour-centage (10/10 = 100%, 5/10 = 50%, etc.). Ce pourcentage sera appli-qué de deux manières :- en supplément au minerval et autres dépenses liées à l’école (machine à café, photocopies, etc.)- en malus sur les points attribués lors des évaluations et des jurys

Exemple :
Si vous obtenez 5/10 :
• 50% du prix de votre minerval vous sera demandé en supplément.
• 50% des points de votre jury seront déduits de votre note finale.
• et ainsi de suite, ...

Article 4 : Références et cours

Article 34 bis. La direction impose désormais au corps enseignant de revoir ses références historiques et artistiques, en vue d’interdire, dans tous les cours théoriques, la mention d’œuvres d’hommes blancs cisgenres hété-rosexuels. L’erg entend ainsi élargir les réfé-rences des étudiantEs et des professeurEs.Les sujets dits «tabous» en ce qu’ils ne sont volontairement pas abordés dans les cours actuels cesseront d’être invisibilisés. On leur rendra leur place réelle dans le paysage culturel. À titre d’exemple, la pornographie, qui représente une partie importante de la production d’images et de récits, sera à réintégrer aux cours appropriés.Les nominations des nouveaux sujets seront les suivantes :• Théorie de la communication misogyne• Histoires et actualités proto Queer et Queer contemporaines• Soutien à l’orientation : les modulations d’incarnat• Récits et expérimentations Post-porn & Ecosex• CV Dazzle, politique des multiples : cours de transformation de l’apparence• Eat your make-up : cours de cui-sine, comment se réapproprier nos moyens de subsistance• Femme Feral : cours de catch, mené par des personnes Queer https://www.youtube.com/watch?v=tgvqEcS4OqA• Déglingue-moi ça ! : cours de maquillage trash invitant les hauts lieux Queer européens pour gérer les propositions.Les orientations seront repensées avec de nouvelles dénominations, comme :• Installation/Performances sexuelles• Sculpture végane• Bander Ciné.e• ...

Article 5 : Organisation des étudesLe rythme des premiers et deuxièmes cycles sont à adapter au cycle menstruel.

Article Cis : Composition des jurysLes jurys sont composés d’au moins cinq membres, dont cinq femmes. Aucune universitaire. Aucune artiste.La composition des jurys sera établie suivant un coefficient inversé établi à l’Article 3 de ce présent document.En préparation, les membres du jurys non-initié.e.s aux questions de genres, de post-colonialisme, de féminismes intersectionnels, queer et situés, devront passer au moins deux semaines enfermé/e/s dans la bibliothèque (et pas aux archives).

Article 7 : Occupation de l’espace et du tempsAfin d’éviter que l’espace commun reproduise les mêmes schémas oppressifs que ceux du monde qui nous entoure, plusieurs règles seront mises en place concernant l’occupation de l’espace :•  la proportion d’hommes cisgenres hétérosexuels blancs ne devra pas excéder 30% pour chaque unité spatiale (salle, couloir, atelier, etc.). Lorsque ce pourcentage est dépassé, l’un ou plusieurs d’entre eux devront quitter la pièce afin de conserver une mixité saine.
• cette règle s’applique également à la prise de parole dans les assemblées mixtes : le temps de parole des hommes cisgenres hétérosexuels blancs ne doit pas dépasser 30% du temps total de la discussion.

Article 8 : Règles de conduite pour les événements publicsPour s’assurer que les personnes s’identifiant comme hétérosexuel.le.s respectent les mêmes règles que les autres lors des événe-ments publics, la pancarte suivante, qui est extraite du Queer Nation Manifesto, devra être affichée à l’entrée de l’espace commun:

Règles de conduite pour les hétéros:
1. Restreignez vos démonstrations d’affection (s’embrasser, se tenir la main, se serrer dans les bras) au minimum. Votre sexualité n’est pas la bienvenue et offensante pour beaucoup ici.
2. Si vous devez danser un slow, soyez le plus discret possible.
3. Ne fixez pas les gouines et les pédés, par-ticulièrement les butchs et les drag queens, nous ne sommes pas vos bêtes de foire.
4. Si vous ne pouvez pas gérer que quelqu’un.e du même sexe vous drague, sortez. 
5. N’affichez pas votre hétérosexualité. Soyez discret.e. Prenez le risque d’être pris.e pour une gouine ou un pédé.
6. Si vous trouvez que ces règles ne sont pas justes, allez vous battre contre l’homophobie dans les boîtes hétéros, ou
7. Allez vous faire enculer. (ça vous fera du bien)Chacun.e sera tenu.e d’observer ces règles et sera tenu.e pour seul.e responsable le cas échéant. Ces règles seront également valables lors des journées portes ouvertes et autres événements publics d’enver-gure, et ce aussi bien par les membres de l’école que par les visiteur.euse.s

Article 9 : Les journées talons et/ou à poilsLes journées talons et/ou à poils imposent le port ostensible du talon et/ou de la pilosité corporelle.Le jour des remises de diplômes devient dès à présent une journée “talons et/ou à poils” dans le but de faire école en rayonnant.Les chaussures et chaussettes laissées en consignes seront soignées et prises en charge pendant la journée par des fétichistes des pieds (une offre publique sera envoyée pour l’occasion).

Article 10 : Toilettes
Les toilettes de l’erg seront à présent renommées pour des usages alternatifs :• toilettes pour véganes et cannibales • toilettes pour maniaques et fées du logis• toilettes pour les hommes qui ont leur règles et femmes qui font pipi deboutArticle 10 biLes représentations dessinées d’organes sexuels dans les toilettes (et ailleurs) doivent être variées.

Article 10 bis : L’école fournira des distributeurs gra-tuits de tampons, serviettes et mooncup, de pisse-debouts, de préservatifs de toutes sortes et de gels, dans toutes les toilettes (y compris les véganes).Cette liste peut être à tout moment mise à jour lors du conseil des toilettes.

Article 11 : Cuddle RoomUne Cuddle Room sera mise à disposi-tion pendant toute l’année à l’école. Des matelas et coussins seront installés dans cet espace pour se caliner.Le consentement est de mise et les rapports sexuels sont autorisés dans cet espace à la condition de se déplacer dans la Back Room.

Article 12  : Respect et rapport au matériel          
Sont organisées régulièrement des soirées Queer et bondage, où toute personne entrant dans le bâtiment doit s’attacher à des meubles, des murs, des portes dans l’école et y passer la nuit.(voir : Furniture Porn Project – Antoine Heraly (Fun Queer), les chaînes humaines écoféministes de Greenham Common, ...)

Article 13 : Visite médicaleL’erg refuse l’imposition de la visite médicale obligatoire et propose une séance d’auto-santé et d’auto-observation gynéco-andro-génitale, pour la ré-appropriation des connaissances de nos corps.


